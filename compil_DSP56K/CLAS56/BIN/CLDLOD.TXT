/* $Id: cldlod.txt,v 1.3 2000/02/09 00:15:42 jay Exp $ */
cldlod, Version 6.3.1, Feb 8 2000
1. Added support for 56600 pb, pc and pd memory.
====
cldlod, Version 6.3, Mar 19 1999
1. Added support for sc100 device.
====
cldlod, Version 6.2, Jan 06 1998
1. The only change is to the version number, in preparation for
the CD distribution release.
====


